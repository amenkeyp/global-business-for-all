package tg.ecommerce.project.global.business.formation.controlers;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tg.ecommerce.project.global.business.formation.beans.Etudiant;


@RestController
@RequestMapping("gerer/api/v1/etudiants")
public class GererEtudiantController {
	
	private static final List<Etudiant> etudiants = Arrays.asList(
			new Etudiant(1,"JAVA_COURSE","kkhome"),
			new Etudiant(2,"PHP_COURSE","hghome"),
			new Etudiant(3,"PYTHON_COURSE","oiohome")
			);
	@GetMapping
	@PreAuthorize("hasAnyRole('ROLE_ADMINISTRATEUR', 'ROLE_PROFESSEUR')")
	public List<Etudiant> getAllEtudiants(){
		return etudiants;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('etudiant:ecrire')")
	public void enregistrerEtudiant(@RequestBody Etudiant etudiant) {
		System.out.println("Student -------------");
	}
	
	@DeleteMapping(path ="{etudiantId}")
	@PreAuthorize("hasAuthority('etudiant:ecrire')")
	public void supprimerEtudiant(@PathVariable("etudiantId") Integer etudiantId) {
		System.out.println("Suppimer etudiant");
		
	}
	
	@PutMapping(path = {"etudiantId"})
	@PreAuthorize("hasAuthority('etudiant:ecrire')")
	public void modifierEtudiant(@PathVariable("etudiantId") Integer etudiantId, Etudiant etudiant) {
		System.out.println("Modifier Etudiant");
		
	} 
}
