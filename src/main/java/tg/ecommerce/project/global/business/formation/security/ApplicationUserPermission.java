package tg.ecommerce.project.global.business.formation.security;

public enum ApplicationUserPermission {
	
	ETUDIANT_LIRE("etudiant:lire"),
	ETUDIANT_ECRIRE("etudiant:ecrire"),
	COURS_LIRE("cours:lire"),
	COURS_ECRIRE("cours:ecrire");
	
	private final String permission;	

	private ApplicationUserPermission(String permission) {
		this.permission = permission;
	}

	public String getPermission() {
		return permission;
	}

}
