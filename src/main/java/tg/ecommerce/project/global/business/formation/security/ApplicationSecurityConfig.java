package tg.ecommerce.project.global.business.formation.security;

import org.springframework.beans.factory.annotation.Autowired;
import static tg.ecommerce.project.global.business.formation.security.ApplicationUserRole.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private final PasswordEncoder passwordEncoder;
	
	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		    .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
		    .and()
		    .authorizeRequests()
		    .antMatchers("/","index","/css/*","/js/*")
		    .permitAll()
		    .antMatchers("/api/**").hasRole(ETUDIANT.name())
		    /**.antMatchers(HttpMethod.DELETE, "/gerer/api/**").hasAuthority(COURS_ECRIRE.getPermission())
		    .antMatchers(HttpMethod.POST, "/gerer/api/**").hasAuthority(COURS_ECRIRE.getPermission())
		    .antMatchers(HttpMethod.PUT, "/gerer/api/**").hasAuthority(COURS_ECRIRE.getPermission())
		    .antMatchers("/gerer/api/**").hasAnyRole(ADMINISTRATEUR.name(),PROFESSEUR.name())**/
		    .anyRequest()
		    .authenticated()
		    .and()
		    //.httpBasic();
		    .formLogin()
		    .loginPage("/login").permitAll();
	}
	
	@Override
	@Bean
	protected UserDetailsService userDetailsService() {
	  UserDetails annasmith=User
	      .builder()
	      .username("annasmith")
	      .password(passwordEncoder.encode("password")) 
	     // .roles(ETUDIANT.name())
	      .authorities(ETUDIANT.getGrantedAuthorities())
	      .build();
	  
	  UserDetails pierre = User
		  .builder()
		  .username("pierre")
		  .password(passwordEncoder.encode("pierre"))
		  .authorities(ADMINISTRATEUR.getGrantedAuthorities())
		 // .roles(ADMINISTRATEUR.name())
		  .build();
	  
	  UserDetails koffi = User
		  .builder()
		  .username("Koffi")
		  .password(passwordEncoder.encode("password"))
		  .authorities(PROFESSEUR.getGrantedAuthorities())
		 //  .roles(PROFESSEUR.name())
		  .build();
	  
	  return new InMemoryUserDetailsManager(
			  annasmith,
			  pierre,
			  koffi
			  );
	}

}
