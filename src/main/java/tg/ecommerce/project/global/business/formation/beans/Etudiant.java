package tg.ecommerce.project.global.business.formation.beans;

public class Etudiant {
	
	private   Integer etudiantId;
	private  String name;
	private  String url;
	
	
	
	public Etudiant() {

	}
	public Etudiant(Integer etudiantId, String name, String url) {
		this.etudiantId = etudiantId;
		this.name = name;
		this.url = url;
	}
	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public Integer getEtudiantId() {
		return etudiantId;
	}
	

	
}
