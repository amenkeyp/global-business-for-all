package tg.ecommerce.project.global.business.formation.security;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets;
import static tg.ecommerce.project.global.business.formation.security.ApplicationUserPermission.*;

public enum ApplicationUserRole {

	ETUDIANT(Sets.newHashSet()), PROFESSEUR(Sets.newHashSet(COURS_ECRIRE, ETUDIANT_ECRIRE)), ADMINISTRATEUR(
			Sets.newHashSet(COURS_LIRE, COURS_ECRIRE, ETUDIANT_LIRE, ETUDIANT_ECRIRE));

	private final Set<ApplicationUserPermission> permissions;

	private ApplicationUserRole(Set<ApplicationUserPermission> permissions) {
		this.permissions = permissions;
	}

	public Set<ApplicationUserPermission> getPermissions() {
		return permissions;
	}

	public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
		Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
				.map(permission -> new SimpleGrantedAuthority(permission.getPermission())).collect(Collectors.toSet());
		permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
		return permissions;
	}

}
