package tg.ecommerce.project.global.business.formation.controlers;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tg.ecommerce.project.global.business.formation.beans.Etudiant;
@RestController
@RequestMapping("api/v1/etudiants")
public class EtudiantController {
	
	private static final List<Etudiant> etudiants = Arrays.asList(
			new Etudiant(1,"JAVA_COURSE","kkhome"),
			new Etudiant(2,"PHP_COURSE","hghome"),
			new Etudiant(3,"PYTHON_COURSE","oiohome")
			);
	
	@GetMapping(path="{etudiantId}")
	public Etudiant getEtudiantId(@PathVariable("etudiantId") Integer etudiantId) {
		return etudiants.stream()
			            .filter(etudiant -> etudiantId.equals(etudiant.getEtudiantId()))
			            .findFirst()
			            .orElseThrow(()->new IllegalStateException("Etudiant " + etudiantId));
	}

}
